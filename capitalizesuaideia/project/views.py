# Create your views here.
from capitalizesuaideia.project.forms import ProjectForm
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.contrib.auth.decorators import login_required

@login_required
def createproject(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data['long_description']
            return HttpResponse('%s'%(data))
    else:
        form = ProjectForm()
    return render_to_response('submitproject.html', {
        'form': form,
        }, context_instance=RequestContext(request))

def viewproject(request, identification):
    try:
        ident = int(identification)
    except:
        raise Http404()
    
    user = request.user
    project = Project.objects.get(id=ident)
    variables = Context({'project': project})
    return HttpResponse('%s'%(project.body))
    #return render_to_response('teste.html', variables)