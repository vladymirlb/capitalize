"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from project.models import Project
from django.contrib.auth.models import User
from account.models import UserProfile
import datetime

class ProjectTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user('foo', 'vlad@mybox.net', 'bar')
                                    
        self.user1 = UserProfile.objects.create(user=user, name="vlad", surname="bezerra", 
                                                address="Rua Tal",
                                                zipcode=58432045,
                                                code=83,
                                                phone=96547177)
        self.user1.save()
        self.projeto = Project.objects.create(owner=self.user1, title='Projeto de Musica',
                                             slug='Projeto de Musica', body='lalalalalalall',
                                             expire=datetime.datetime.now(),
                                             status='P', category='Musica')
        self.projeto.save()                                     
        
    def test_project(self):
        self.assertEqual(self.projeto.owner.name, 'vlad')
        self.assertEqual(self.projeto.title, 'Projeto de Musica')
        self.assertEqual(self.projeto.slug, 'Projeto de Musica')
        self.assertEqual(self.projeto.body, 'lalalalalalall')
        self.assertEqual(self.projeto.status, 'P')
            
    def test_change_status(self):
        self.projeto.status='A'
        self.projeto.save()
            
