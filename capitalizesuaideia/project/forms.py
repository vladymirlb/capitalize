from django import forms
from django.utils.translation import ugettext_lazy

class ProjectForm(forms.Form):
    """
        Event data form
    """
    min_description_chars = 100
    max_description_chars = 2000

    ltitle = ugettext_lazy(u"Titulo")    
    ltitle_help = ugettext_lazy(u"O titulo deste projeto.")
    ltitle_required = ugettext_lazy(u"Por favor digite o titulo.")
    
    ldescription = ugettext_lazy(u"Descricao")
    ldescription_help = ugettext_lazy(u"Entre com a descricao do projeto")
    ldescription_required = ugettext_lazy(u"Por favor descreva este projeto.")    
    ldescription_min_length = ugettext_lazy(u'O numero minimo de caracteres e %s')%(min_description_chars)
    ldescription_max_length = ugettext_lazy(u'O numero maximo de caracteres e %s')%(max_description_chars)

    id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    title = forms.CharField(max_length=60, required=True, label=ltitle, help_text=ltitle_help, error_messages={'required': ltitle_required})
    long_description = forms.CharField(widget=forms.Textarea(attrs={'cols': 50, 'rows': 5}), min_length=min_description_chars, max_length=max_description_chars, required=True, label=ldescription, help_text=ldescription_help)