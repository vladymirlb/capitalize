from django.db import models
from django.db.models import permalink
from account import models as accmodels
from django.contrib.auth.models import User
# Create your models here.

CATEGORY_CHOICES = (
        ('Arte', 'Arte'),
        ('Jogos', 'Jogos'),
        ('Moda', 'Moda'),
        ('Filme e Video', 'Filme e Video'),
        ('Gastronomia', 'Gastronomia'),
        ('Publicacoes', 'Publicacoes'),
        ('Danca', 'Danca'),
        ('Teatro', 'Teatro'),
        ('Musica', 'Musica'),
        ('Fotografia', 'Fotografia'),
        ('Design', 'Design'),
        ('Tecnologia','Tecnologia')
    )
STATUS_CHOICES = (
        ('N', 'NEGADO'),
        ('P', 'PENDENTE'),
        ('A', 'ACEITO')
    )    
class Project(models.Model):
    
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    body = models.TextField()
    posted = models.DateTimeField(db_index=True, auto_now_add=True)
    expire = models.DateTimeField(db_index=True)
    num_days = models.IntegerField()
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)
    category = models.CharField(max_length=20, choices=CATEGORY_CHOICES, blank=False)

    def __unicode__(self):
        return '%s' % self.title

    @permalink
    def get_absolute_url(self):
        return('view_project_post', None, { 'slug': self.slug })


from django.core.mail import send_mail
from django.dispatch import receiver
from django.db.models.signals import post_save

@receiver(post_save, sender=Project)
def my_handler(sender, **kwargs):
    project = kwargs['instance']
    user = project.owner
    if project.status=='A':
        send_mail('[CapitalizeSuaIdeia] Projeto Aceito', 'Seu projeto foi aceito por nossa equipe. Parabens', 'admin@capitalizesuaideia.com.br', [user.user.email], fail_silently=False)
    elif project.status == 'N':
        send_mail('[CapitalizeSuaIdeia] Projeto Negado', 'Seu projeto foi negado', 'admin@capitalizesuaideia.com.br', [user.user.email], fail_silently=False)
        