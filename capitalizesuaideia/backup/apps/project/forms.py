from django import forms
from django.utils.translation import ugettext_lazy

class ProjectForm(forms.Form):
    """
        Event data form
    """
    min_description_chars = 100
    max_description_chars = 2000

    ltitle = ugettext_lazy("Title")    
    ltitle_help = ugettext_lazy("The title of the event.")
    ltitle_required = ugettext_lazy("Please enter the title.")
    
    ldescription = ugettext_lazy("Description")
    ldescription_help = ugettext_lazy("Enter the event description.")
    ldescription_required = ugettext_lazy("Please describe the event.")    
    ldescription_min_length = ugettext_lazy('The minimum length is %s characters.')%(min_description_chars)
    ldescription_max_length = ugettext_lazy('The maximum length is %s characters of plain text (less characters for formated text)')%(max_description_chars)

    id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    title = forms.CharField(max_length=60, required=True, label=ltitle, help_text=ltitle_help, error_messages={'required': ltitle_required})
    long_description = forms.CharField(widget=forms.Textarea(attrs={'cols': 50, 'rows': 5}), min_length=min_description_chars, max_length=max_description_chars, required=True, label=ldescription, help_text=ldescription_help)