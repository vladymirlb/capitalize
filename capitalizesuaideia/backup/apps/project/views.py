# Create your views here.
from capitalizesuaideia.project.forms import ProjectForm
from django.shortcuts import render_to_response

def createproject(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/')
    else:
        form = ProjectForm()

    return render_to_response('submitproject.html', {
        'form': form,
    })
