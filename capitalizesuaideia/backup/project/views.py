# Create your views here.
from capitalizesuaideia.project.forms import ProjectForm
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required

@login_required
def createproject(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/')
    else:
        form = ProjectForm()
    return render_to_response('submitproject.html', {
        'form': form,
        }, context_instance=RequestContext(request))
