"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from account.models import UserProfile

class TestUserProfile(TestCase):
    def setUp(self):
        self.user1 = UserProfile.objects.create(name="vlad", surname="bezerra", 
                                                address="Rua Tal",
                                                zipcode=58432045,
                                                code=83,
                                                phone=96547177
                                                )
        self.user2 = UserProfile.objects.create(name="capitalize", 
                                surname="ideia",
                                address="Rua Til n.115",
                                zipcode=63260000)
    def test_user_profile(self):
        self.assertEqual(self.user1.name, "vlad")
        self.assertEqual(self.user1.surname, "bezerra")
        self.assertEqual(self.user1.address, "Rua Tal")
        self.assertEqual(self.user1.zipcode, 58432045)
        self.assertEqual(self.user1.code, 83)
        self.assertEqual(self.user1.phone, 96547177)

class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)
