from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User)

    name = models.CharField(max_length=75)
    surname = models.CharField(max_length=75)
    address = models.CharField(max_length=200)
    complement = models.CharField(max_length=100)
    zipcode = models.IntegerField()
    code = models.IntegerField(max_length=2)
    phone = models.IntegerField(max_length=8)
    shortbio = models.TextField(max_length=500)
    photo = models.ImageField(upload_to="/media/", blank=True)
    
    def get_absolute_url(self):
        return ('profiles_profile_detail', (), { 'username': self.user.username })
    get_absolute_url = models.permalink(get_absolute_url)