"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from account.models import UserProfile
from project.models import Project
from django.contrib.auth.models import User
import datetime

class TestUserProfile(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('foo', 'foo@bar', 'bar')
                                    
        self.user1 = UserProfile.objects.create(user=self.user, name="vlad", surname="bezerra", 
                                                address="Rua Tal",
                                                zipcode=58432045,
                                                code=83,
                                                phone=96547177)
        
                                                
            
    def test_user_profile(self):
        self.assertEqual(self.user1.user.username, 'foo')
        self.assertEqual(self.user1.name, "vlad")
        self.assertEqual(self.user1.surname, "bezerra")
        self.assertEqual(self.user1.address, "Rua Tal")
        self.assertEqual(self.user1.zipcode, 58432045)
        self.assertEqual(self.user1.code, 83)
        self.assertEqual(self.user1.phone, 96547177)
