from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, unique=True)

    name = models.CharField(max_length=75)
    surname = models.CharField(max_length=75)
    address = models.CharField(max_length=200)
    complement = models.CharField(max_length=100)
    zipcode = models.IntegerField()
    code = models.IntegerField(max_length=2)
    phone = models.IntegerField(max_length=8)
    shortbio = models.TextField(max_length=500)
    photo = models.ImageField(upload_to="/media/", blank=True)
    
    def __unicode__(self):
        return '%s %s' % (self.name, self.surname)
    
    def get_absolute_url(self):
        return ('profiles_profile_detail', (), { 'username': self.user.username })
    get_absolute_url = models.permalink(get_absolute_url)
    
#@receiver(post_save, sender=User)
#def create_profile(sender, instance, created, **kwargs):
    #if created:
        #UserProfile.objects.get_or_create(user=instance)
      