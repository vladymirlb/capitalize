from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.views.generic.simple import direct_to_template
from registration.forms import RegistrationFormUniqueEmail
from capitalizesuaideia.project import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'capitalizesuaideia.views.home', name='home'),
    # url(r'^capitalizesuaideia/', include('capitalizesuaideia.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/register/$', 'registration.views.register',
    {'form_class':RegistrationFormUniqueEmail,
        'backend':'registration.backends.default.DefaultBackend' }, name='registration_register'),
    url(r'^$', direct_to_template,
            { 'template': 'index.html' }, 'index'),
    url(r'^profiles/', include('profiles.urls')),
    url(r'^project/(\d+)/$', views.viewproject),
    url(r'^project/new/$', views.createproject),
)

if settings.SERVE_STATIC_FILES:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', 
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )
    # staticfiles
    urlpatterns += staticfiles_urlpatterns()

